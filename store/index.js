export const state = () => ({
    accessToken: ""
})

export const mutations = {
    setToken(state, token) {
        state.accessToken = token
    }
}