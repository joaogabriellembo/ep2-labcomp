export function validateEmail(email) {
    if (email.length === 0)
        return {status: "error", errors: ["The field email cannot be empty."]};

    if (!email.match(".+@.+[.].+"))
        return {status: "error", errors: ["Insert a valid email."]};

    return {status: "success"}
}

export function validatePassword(password) {
    const errors = [];

    if (password.length === 0)
        return {status: "error", errors: ["The field password cannot be empty."]};

    if (password.length < 8 || password.length > 32)
        errors.push("Your password must have between 8 and 32 characters.");

    if (!password.match("^[A-z0-9]+$"))
        errors.push("Your password must only have alphanumeric characters.");

    if (errors.length === 0)
        return {status: "success"};

    return {status: "error", errors};    
}

export function validateUsername(username) {
    const errors = [];
    if (username.length === 0)
        return {status: "error", errors: ["The field username cannot be empty."]}

    if (username.substring(0,1).match("[0-9]"))
        errors.push("Your username must not start with a number.");

    if (!username.match("^[A-z0-9]+$"))
        errors.push("Your username must only have alphanumeric characters.");

    if (errors.length === 0)
        return {status: "success"};

    return {status: "error", errors};
}